package pl.arma.tud_jdbc.util;

public class NameUtils {
	public static String camelToSnakeCase(String name)
	{
		String snakeCasedName = "";
		
		for (int i = 0; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if (Character.isUpperCase(c))
				snakeCasedName += (snakeCasedName.length() == 0 ? "" : "_") + Character.toLowerCase(c);
			else
				snakeCasedName += c;
		}
		
		return snakeCasedName;
	}
	
	public static String snakeToCamelCase(String name) {
		String camelCasedName = "";
		String[] nameParts = name.split("_");
		
		for (String part : nameParts) {
			if (camelCasedName.length() == 0)
				camelCasedName += part;
			else {
				char c = part.charAt(0);
				if (part.length() > 1)
					camelCasedName += Character.toUpperCase(c) + part.substring(1);
				else
					camelCasedName += Character.toUpperCase(c);
			}
		}
		
		return camelCasedName;
	}
	
	public static String capitalize(String name) {
		if (name.length() > 0) {
			String capitalizedName = new String();
			
			capitalizedName += Character.toUpperCase(name.charAt(0));
			if (name.length() > 1)
				capitalizedName += name.substring(1);

			return capitalizedName;
		}
		else return name;
	}
}
