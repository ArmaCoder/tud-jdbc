package pl.arma.tud_jdbc.db;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import pl.arma.tud_jdbc.db.entity.DbEntity;
import pl.arma.tud_jdbc.util.NameUtils;

public abstract class TableManager<T> {
	private static String SELECT_STATEMENT = "SELECT * FROM {table_name} ";
	private static String INSERT_STATEMENT = "INSERT INTO {table_name} ({columns}) VALUES ({values})";
	private static String UPDATE_STATEMENT = "UPDATE {table_name} SET ({columns}) = ({values}) WHERE {criteria}";
	private static String DELETE_STATEMENT = "DELETE FROM {table_name} WHERE {criteria}";
	
	// This will hold a "this entity <-> a list of referring entities" mapping
	protected static HashMap<String, ArrayList<String>> references = new HashMap<>(); 

	protected DbManager dbManager;
	protected Connection hsqlConnection;
	protected Class<T> targetClass;
	protected String targetEntityName;
	
	public TableManager(DbManager dbmgr, Class<T> tc) {
		dbManager = dbmgr;
		hsqlConnection = dbmgr.getConnection();
		targetClass = tc;
		targetEntityName = targetClass.getSimpleName();
		
		// Automatically search for referring entities
		mapReferences();
	}
	
	// This method searches for foreign keys
	// in this entity class and lists them
	// all in the array of references.
	private void mapReferences() {
		Method[] entityMethods	= targetClass.getDeclaredMethods();
		
		// Iterating through all declared methods from
		// this entity class
		for (Method m : entityMethods) {
			String methodName = m.getName();
			// Selecting all getters that return keys
			if (methodName.startsWith("get") && methodName.endsWith("Id")) {
				// The name of an entity that refers to this one
				String refEntityName = methodName.substring(3, methodName.length() - 2);
				ArrayList<String> refDependants = null;
				// Selecting only the names of referring classes
				// (there is obviously a getter returning the PK
				// of this entity)
				if (!refEntityName.equalsIgnoreCase(targetEntityName)) {
					if (references.containsKey(refEntityName))
						refDependants = references.get(refEntityName);
					else {
						refDependants = new ArrayList<>();
						references.put(refEntityName, refDependants);
					}
					
					refDependants.add(targetClass.getSimpleName());
				}
			}
		}
	}
	
	// This method tries to recreate a table
	// if it does not exist. Because every
	// table has its own structure, this is
	// left out as abstract.
	public abstract void tryRecreateTable();
	
	// This is only a convenience method.
	// It just calls get(Integer, Boolean).
	public ArrayList<T> get() {
		return get(null, null);
	}
	
	// This is only a convenience method.
	// It just calls get(Integer, Boolean).
	public ArrayList<T> get(Integer id) {
		return get(id, null);
	}
	
	// This generic method returns a specific record,
	// or all records from the table.
	// The idIsFK parameter controls whether or not
	// the searched record is from another table
	// referencing this one.
	// This uses Reflection API to build and
	// populate Java objects (entities) with data.
	public ArrayList<T> get(Integer id, Class<? extends DbEntity> searchedTableClass) throws IllegalArgumentException {
		ArrayList<T> requestedRecords = new ArrayList<>();
		
		// Preparing a statement
		String tableName 		= targetEntityName.toLowerCase();
		String primaryKeyName 	= tableName + "_id";
		String selectString		= SELECT_STATEMENT;
		
		// Select a specific record if possible
		if (id != null) {
			// If a DB entity class was provided,
			// this piece of code checks whether or 
			// not there actually is a reference to
			// this class in the class provided
			if (searchedTableClass != null) {
				String searchedClassName = searchedTableClass.getSimpleName();
				ArrayList<String> allReferences = references.get(searchedClassName);

				// If there is a reference to this class
				// in searchedTableClass, change the table
				// we select data from to searchedTableClass
				if (allReferences != null && allReferences.contains(targetEntityName))
					primaryKeyName = searchedClassName.toLowerCase() + "_id";	
				else
					throw new IllegalArgumentException(searchedClassName + " is not related to " + targetEntityName);
			}
			
			selectString += "WHERE " + primaryKeyName + " = " + id;
		}
		
		// Insert the searched table name into the query
		selectString = selectString.replace("{table_name}", tableName);
		
		try {
			Statement selectStatement = hsqlConnection.createStatement();
			ResultSet results = selectStatement.executeQuery(selectString);
			
			while (results.next()) {
				// An instance of the given entity class.
				T record = targetClass.newInstance();
				// Retrieving a list of setters for the entity class.
				Method[] targetMethods = record.getClass().getDeclaredMethods();
				// Iterating through all setters and setting
				// them with data pulled from the DB.
				for (Method m : targetMethods) {
					String methodName = m.getName();
					// Handling the setters only.
					if (methodName.startsWith("set")) {
						String columnName = NameUtils.camelToSnakeCase(m.getName().substring(3));
						// Invoking the setter method with the data.
						m.invoke(record, results.getObject(columnName, m.getParameterTypes()[0]));
					}
				}
				requestedRecords.add(record);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return requestedRecords;
	}
	
	// This method inserts (if PK is null) or updates 
	// (if PK is not null) an entity in the DB. 
	// This uses Reflection API to build and
	// populate a PreparedStatement with data.
	public int insertOrUpdate(T entity) {
		String tableName 		= targetEntityName.toLowerCase();
		String columnNames		= "";
		String values			= "";
		int affectedRows		= 0;
		
		try {
			PreparedStatement insertStatement = null;
			
			// All declared methods from the class
			Method[] entityMethods = entity.getClass().getDeclaredMethods();
			// This will hold all the values the getters return.
			ArrayList<Object> methodValues = new ArrayList<>();
			
			// This loop iterates through all the getters
			// from the entity class being inserted, builds
			// the lists of field names and corresponding ?'s,
			// and stores retrieved entity values in an array. 
			for (Method m : entityMethods) {
				String methodName = m.getName();
				
				if (methodName.startsWith("get")) {
					String fieldName = NameUtils.camelToSnakeCase(methodName.substring(3));
					// If a field's name end's with "_id" - it's a key.
					// If what's before that equals this table's name,
					// it's the primary key, and we don't need it.
					if (!(fieldName.endsWith("_id") && 
						fieldName.split("_")[0].equals(tableName)))
					{
						columnNames += (columnNames.length() == 0 ? "" : ",") + fieldName;
						values += (values.length() == 0 ? "" : ",") + "?";
						
						// Invoking the getter through the magic
						// of the Reflection API and putting the
						// return value into the array.
						methodValues.add(m.invoke(entity));
					}
				}
			}
			
			// Getting the PK of the possibly updated record
			String getPKMethodName	= "get" + targetEntityName + "Id";
			String pkFieldName		= tableName + "_id";
			Method getPKMethod		= targetClass.getMethod(getPKMethodName, null);
			Integer pk				= (Integer) getPKMethod.invoke(entity);
			
			// Inserting the created parts into the statement
			String queryString = (pk == null ? INSERT_STATEMENT : UPDATE_STATEMENT);
			
			queryString = queryString
					.replace("{table_name}", tableName)
					.replace("{columns}", columnNames)
					.replace("{values}", values)
					.replace("{criteria}", pkFieldName + "=" + pk);
			
			// Creating the actual PreparedStatement
			// Note the RETURN_GENERATED_KEYS option
			insertStatement = hsqlConnection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS);
			
			// Inserting the returned values into the statement
			for (int argIndex = 1; argIndex <= methodValues.size(); argIndex++) {
				insertStatement.setObject(argIndex, methodValues.get(argIndex - 1));
			}
			
			// Try inserting that now...
			affectedRows = insertStatement.executeUpdate();
			if (affectedRows == 1) {
				// Get the setter for ID
				String setPKMethodName	= "set" + targetEntityName + "Id";
				Method setPKMethod		= targetClass.getMethod(setPKMethodName, Integer.class);				
				// Retrieve the auto-generated PK.
				ResultSet generatedKeys	= insertStatement.getGeneratedKeys();
				// Invoke the ID setter method
				if (generatedKeys.next())
					setPKMethod.invoke(entity, generatedKeys.getInt(1));
			}
		}
		catch (Exception e) { }
		
		return affectedRows;
	}
	
	// This removes an entity from DB.
	// Because some tables contain foreign
	// keys, this should be overwritten in
	// classes that extend this one.
	public int delete(T entity) {
		String tableName		= targetEntityName.toLowerCase();
		String primaryKeyName	= tableName + "_id";
		// The name of a getter that will return
		// the primary key value of the entity
		String getIdMethodName	= "get" + targetEntityName + "Id";
		// The total number of affected rows
		int totalAffectedRows	= 0;
		
		try {			
			// Since we might be deleting more than
			// once record, we'll disable auto-commit
			hsqlConnection.setAutoCommit(false);
			
			// Finding the getter using Reflection API,
			// and calling it to retrieve the PK.
			Method getPKMethod = targetClass.getMethod(getIdMethodName, null);
			Integer pk = (Integer) getPKMethod.invoke(entity);
			
			// Return: zero rows updated
			if (pk == null) return 0;
			
			// First delete all records from other tables
			// that refer to the record being deleted
			ArrayList<String> referringEntities = references.get(targetEntityName);
			if (referringEntities != null) {
				for (String refEntityName : referringEntities) {
					// Table names are always lowercase
					String refTableName		= refEntityName.toLowerCase();
					String refDeleteString	= DELETE_STATEMENT
							.replace("{table_name}", refTableName)
							.replace("{criteria}", primaryKeyName + "=" + pk);
					
					totalAffectedRows += hsqlConnection.createStatement().executeUpdate(refDeleteString);
				}
			}
			
			// Now delete the record in question
			String deleteString		= DELETE_STATEMENT.replace("{table_name}", tableName);
			String criteriaString	= primaryKeyName + "=" + pk;
			Statement deleteStatement;
			
			deleteString 	= deleteString.replace("{criteria}", criteriaString);
			deleteStatement	= hsqlConnection.createStatement();
			
			totalAffectedRows += deleteStatement.executeUpdate(deleteString);
			
			// Commit all changes at once
			hsqlConnection.commit();
		}
		catch (Exception e) {
			try {
				hsqlConnection.rollback();
				e.printStackTrace();
			}
			catch (Exception ce) {
				ce.printStackTrace();
			}
			
			totalAffectedRows = 0;
		}
		finally {
			try {
				hsqlConnection.setAutoCommit(true);
			}
			catch (Exception fe) {
				fe.printStackTrace();
			}
		}
		
		return totalAffectedRows;
	}
	
}
