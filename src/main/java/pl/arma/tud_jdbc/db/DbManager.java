package pl.arma.tud_jdbc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class DbManager {
	private static String HSQLDB_URL = "jdbc:hsqldb:hsql://localhost/armadb";
	private Connection hsqlConnection;
	private MangaManager mangaManager;
	private ReviewManager reviewManager;
	
	public DbManager() {
		try {
			hsqlConnection	= DriverManager.getConnection(HSQLDB_URL);
			mangaManager	= new MangaManager(this, Manga.class);
			reviewManager	= new ReviewManager(this, Review.class);
		} 
		catch (SQLException se) {
			System.out.println("B��d po��czenia z baz�:\n" + se.getMessage());
		}
	}
	
	public Connection getConnection() {
		return hsqlConnection;
	}
	
	public MangaManager getMangaManager() {
		return mangaManager;
	}
	
	public ReviewManager getReviewManager() {
		return reviewManager;
	}
}
