package pl.arma.tud_jdbc.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class MangaManager extends TableManager<Manga> {
	// CREATE TABLE statement
	private static String CREATE_STATEMENT = 
			"CREATE TABLE IF NOT EXISTS manga ("
			+ "manga_id INTEGER NOT NULL PRIMARY KEY IDENTITY, "
			+ "title VARCHAR(100) NOT NULL CHECK(LENGTH(title) >= 3), "
			+ "year_of_publication INTEGER NOT NULL CHECK(year_of_publication >= 1990), "
			+ "synopsis VARCHAR(1M) DEFAULT NULL, "
			+ "tags VARCHAR(256) DEFAULT NULL ) ";
	
	
	public MangaManager(DbManager dbmgr, Class<Manga> tc) {
		super(dbmgr, tc);
		
		tryRecreateTable();
	}

	@Override
	public void tryRecreateTable() {		
		try {
			Statement createStatement = hsqlConnection.createStatement();
			createStatement.executeQuery(CREATE_STATEMENT);
		}
		catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	// This method is MangaManager-specific:
	// it allows retrieval of all reviews
	// of the specified Manga
	public ArrayList<Review> getReviews(Manga manga) {
		ReviewManager rmgr = dbManager.getReviewManager();
		ArrayList<Review> allReviews = new ArrayList<>();
		
		if (manga.getMangaId() != null) {
			allReviews = rmgr.get(manga.getMangaId(), Manga.class);
		}
		
		return allReviews;
	}

}
