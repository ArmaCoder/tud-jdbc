package pl.arma.tud_jdbc.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class ReviewManager extends TableManager<Review> {
	// CREATE TABLE statement
	private static String CREATE_STATEMENT =
			"CREATE TABLE IF NOT EXISTS review ("
			+ "review_id INTEGER NOT NULL PRIMARY KEY IDENTITY, "
			+ "manga_id INTEGER NOT NULL FOREIGN KEY REFERENCES manga(manga_id), "
			+ "date_of_posting DATE DEFAULT CURRENT_DATE NOT NULL, "
			+ "title VARCHAR(100) NOT NULL CHECK(LENGTH(title) >= 3), "
			+ "score_for_art DOUBLE NOT NULL CHECK(score_for_art >= 0 AND score_for_art <= 5), "
			+ "score_for_plot DOUBLE NOT NULL CHECK(score_for_plot >= 0 AND score_for_plot <= 5), "
			+ "score_for_dialogues DOUBLE NOT NULL CHECK(score_for_dialogues >= 0 AND score_for_dialogues <= 5), "
			+ "comments VARCHAR(1M) DEFAULT NULL )";

	public ReviewManager(DbManager dbmgr, Class<Review> tc) {
		super(dbmgr, tc);
		
		tryRecreateTable();
	}

	@Override
	public void tryRecreateTable() {		
		try {
			Statement createStatement = hsqlConnection.createStatement();
			createStatement.executeQuery(CREATE_STATEMENT);
		}
		catch (SQLException se) {
			se.printStackTrace();
		}
	}
	
	// This method is ReviewManager-specific:
	// it allows retrieval of a manga associated
	// with the specified Review entity
	public Manga getReviewedManga(Review review) throws Exception {
		MangaManager mmgr = dbManager.getMangaManager();
		
		if (review.getMangaId() != null) {
			ArrayList<Manga> mangaList = mmgr.get(review.getMangaId());
			
			if (mangaList != null && mangaList.size() == 1)
				return mangaList.get(0);
		}
		
		throw new Exception("Couldn't find the manga referenced by review.");
	}

}
