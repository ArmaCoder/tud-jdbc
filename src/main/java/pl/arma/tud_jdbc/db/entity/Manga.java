package pl.arma.tud_jdbc.db.entity;

import java.time.LocalDate;

public class Manga extends DbEntity {
	private Integer mangaId;
	private String title;
	private Integer yearOfPublication;
	private String synopsis;
	private String tags;
	
	@Override
	public boolean equals(Object o) {
		Manga otherManga = (Manga) o;
		
		boolean sameId =	mangaId == null ? 
							otherManga.getMangaId() == null : 
							mangaId.equals(otherManga.getMangaId());
		
		boolean sameTitle =	title == null ?
							otherManga.getTitle() == null :
							title.equals(otherManga.getTitle());
		
		boolean sameYoP	=	yearOfPublication == null ?
							otherManga.getYearOfPublication() == null :
							yearOfPublication.equals(otherManga.getYearOfPublication());
		
		boolean sameSyn =	synopsis == null ?
							otherManga.getSynopsis() == null :
							synopsis.equals(otherManga.getSynopsis());
		
		boolean sameTags =	tags == null ?
							otherManga.getTags() == null :
							tags.equals(otherManga.getTags());
		
		return (sameId && sameTitle && sameYoP && sameSyn && sameTags);
	}

	public Integer getMangaId() {
		return mangaId;
	}

	public void setMangaId(Integer mangaId) throws UnsupportedOperationException {
		if (this.mangaId == null)
			this.mangaId = mangaId;
		else
			throw new UnsupportedOperationException("Cannot change primary keys.");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) throws IllegalArgumentException {
		if (title.length() >= 3)
			this.title = title;
		else
			throw new IllegalArgumentException("The title must be at least 3 chars long");
	}

	public Integer getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(Integer yearOfPublication) throws IllegalArgumentException {
		if (yearOfPublication >= 1990 && yearOfPublication <= LocalDate.now().getYear())
			this.yearOfPublication = yearOfPublication;
		else
			throw new IllegalArgumentException("Year of publication must be between 1990 and now.");
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}
	
}
