package pl.arma.tud_jdbc;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.arma.tud_jdbc.db.DbManager;
import pl.arma.tud_jdbc.db.MangaManager;
import pl.arma.tud_jdbc.db.ReviewManager;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class UpdateTest {
	static DbManager dbMgr;
	static ArrayList<Manga> addedMangas;
	static ArrayList<Review> addedReviews;
	static MangaManager mangaMgr;
	static ReviewManager reviewMgr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbMgr			= new DbManager();
		addedMangas		= new ArrayList<>();
		addedReviews	= new ArrayList<>();
		mangaMgr		= dbMgr.getMangaManager();
		reviewMgr		= dbMgr.getReviewManager();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		if (mangaMgr != null)		
			for (Manga dm : addedMangas)
				mangaMgr.delete(dm);
	}
	
	@Test
	public void test0Connection() {
		assertNotNull(dbMgr.getConnection());
	}
	
	@Test
	public void test1UpdateMangaTest() {
		Manga m			= new Manga();		
		String title	= "The Updated Manga";
		Integer yop		= 2008;
		
		m.setTitle(title);
		m.setYearOfPublication(yop);
		
		if (mangaMgr.insertOrUpdate(m) != 1)
			fail("Could not insert a manga");
		else
			addedMangas.add(m);
		
		// Modifying the title
		String reversedTitle = new StringBuilder(title).reverse().toString();
		
		m.setTitle(reversedTitle);
		
		if (mangaMgr.insertOrUpdate(m) == 1) {
			ArrayList<Manga> mangaList = mangaMgr.get(m.getMangaId());
			
			if (mangaList.size() != 1)
				fail("Something's wrong, could not find the updated manga.");
			else {
				Manga updated = mangaList.get(0);
				
				assertNotNull(updated);
				assertEquals(updated, m);
			}
		}
		else fail("Could not update the manga.");
	}
	
	@Test
	public void test2UpdateReviewTest() {
		// Inserting a manga
		Manga m	= new Manga();
		
		m.setTitle("A Manga That's Been Reviewed");
		m.setYearOfPublication(2012);
		
		if (mangaMgr.insertOrUpdate(m) != 1)
			fail("Could not insert a manga");
		else
			addedMangas.add(m);
		
		// Inserting a review for the manga
		Review r		= new Review();
		String title	= "A review of " + m.getTitle();
		String comments	= "Well worthy of my review!";
		
		r.setMangaId(m.getMangaId());
		r.setTitle(title);
		r.setComments(comments);
		r.setDateOfPosting(Date.valueOf(LocalDate.now()));
		r.setScoreForArt(5.0);
		r.setScoreForDialogues(5.0);
		r.setScoreForPlot(5.0);
		
		if (reviewMgr.insertOrUpdate(r) != 1)
			fail("Could not insert a review");
		else
			addedReviews.add(r);
		
		// Modifying the review
		r.setComments("It's awful!");
		r.setScoreForArt(0.0);
		r.setScoreForDialogues(5.0);
		r.setScoreForPlot(5.0);
		
		if (reviewMgr.insertOrUpdate(r) != 1)
			fail("Could not update a review");
		else {
			ArrayList<Review> reviewList = reviewMgr.get(r.getReviewId());
			if (reviewList.size() == 1)
				assertEquals(r, reviewList.get(0));
			else
				fail("Could not find the updated review");
		}
	}

}
