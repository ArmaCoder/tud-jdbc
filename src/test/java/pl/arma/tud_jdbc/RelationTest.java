package pl.arma.tud_jdbc;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.arma.tud_jdbc.db.DbManager;
import pl.arma.tud_jdbc.db.MangaManager;
import pl.arma.tud_jdbc.db.ReviewManager;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class RelationTest {
	static DbManager dbMgr;
	static ArrayList<Manga> addedMangas;
	static ArrayList<Review> addedReviews;
	static MangaManager mangaMgr;
	static ReviewManager reviewMgr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbMgr			= new DbManager();
		addedMangas		= new ArrayList<>();
		addedReviews	= new ArrayList<>();
		mangaMgr		= dbMgr.getMangaManager();
		reviewMgr		= dbMgr.getReviewManager();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		if (mangaMgr != null)		
			for (Manga dm : addedMangas)
				mangaMgr.delete(dm);
	}
	
	@Test
	public void testReviewAssignment() {		
		// Adding a new manga
		Manga m1 = new Manga();
		
		m1.setTitle("Example manga");
		m1.setYearOfPublication(2006);
		
		if (mangaMgr.insertOrUpdate(m1) != 1)
			fail("Could not insert a manga.");
		else
			addedMangas.add(m1);
		
		// Adding new reviews
		for (int i = 0; i < 3; i++) {
			Review r = new Review();
			
			r.setMangaId(m1.getMangaId());
			r.setTitle("Review number " + (i+1));
			r.setDateOfPosting(Date.valueOf(LocalDate.now()));
			r.setScoreForArt(3.0);
			r.setScoreForPlot(4.0);
			r.setScoreForDialogues(3.5);
			
			if (reviewMgr.insertOrUpdate(r) != 1)
				fail("Could not add a review");
			else
				addedReviews.add(r);
		}
		
		// Checking if all of them 
		// are listed in the list that
		// the getReviews() method returns
		int totalMatched = 0;
		for (Review r : mangaMgr.getReviews(m1)) {
			if (addedReviews.contains(r)) totalMatched++;
		}
		
		// The total of matched reviews
		// should be equal to the size of 
		// the whole array
		assertEquals(addedReviews.size(), totalMatched);
		
		// Creating a second manga
		Manga m2 = new Manga();
		
		m2.setTitle("Second manga");
		m2.setYearOfPublication(2008);
		
		if (mangaMgr.insertOrUpdate(m2) != 1)
			fail("Could not insert the second manga.");
		else
			addedMangas.add(m2);
		
		// Re-assigning the first review
		// to the second manga
		Review r2 = addedReviews.get(0);
		
		r2.setMangaId(m2.getMangaId());
		
		if (reviewMgr.insertOrUpdate(r2) != 1)
			fail("Could not update a review");
		
		// Checking if the review actually
		// got reassigned
		ArrayList<Review> firstMangasReviews = mangaMgr.getReviews(m1);
		ArrayList<Review> secondMangasReviews = mangaMgr.getReviews(m2);
		
		// Checking if the review is still
		// assigned to the first manga
		// (should NOT be at this point)
		for (Review r : firstMangasReviews) {
			if (r.getReviewId().equals(r2.getReviewId()))
				fail("The review has not been reassigned!");
		}
		
		// Checking if the review is
		// assigned to the second manga
		boolean wasReassigned = false;
		
		for (Review r : secondMangasReviews) {
			if (r.equals(r2)) {
				wasReassigned = true;
				break;
			}
		}
		
		assertTrue(wasReassigned);
	}
	
	@Test
	public void testReviewDeletion() {
		// Adding a new manga
		Manga m1 = new Manga();
		
		m1.setTitle("Deletion Example Manga");
		m1.setYearOfPublication(2000);
		
		if (mangaMgr.insertOrUpdate(m1) != 1)
			fail("Could not insert a manga.");
		else
			addedMangas.add(m1);
		
		// Adding a review
		Review r1 = new Review();
		
		r1.setMangaId(m1.getMangaId());
		r1.setTitle("Soon to be deleted");
		r1.setDateOfPosting(Date.valueOf(LocalDate.now()));
		r1.setScoreForArt(5.0);
		r1.setScoreForPlot(5.0);
		r1.setScoreForDialogues(5.0);
		
		if (reviewMgr.insertOrUpdate(r1) != 1)
			fail("Could not insert a review");
		
		// Checking that the review is
		// listed as the only one by the
		// getReviews() method
		ArrayList<Review> rList = mangaMgr.getReviews(m1);
		
		assertTrue(rList.size() == 1 && rList.get(0).equals(r1));
		
		// Removing the review from the DB
		if (reviewMgr.delete(r1) != 1)
			fail("Could not delete the review");
		
		// Checking once again to see that
		// the review is not listed by the
		// getReviews() method anymore
		ArrayList<Review> newRList = mangaMgr.getReviews(m1);
		
		assertTrue(newRList.size() == 0);
	}

}
