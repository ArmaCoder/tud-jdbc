package pl.arma.tud_jdbc;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import pl.arma.tud_jdbc.db.DbManager;
import pl.arma.tud_jdbc.db.MangaManager;
import pl.arma.tud_jdbc.db.ReviewManager;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InsertionTest {
	static DbManager dbMgr;
	static ArrayList<Manga> addedMangas;
	static ArrayList<Review> addedReviews;
	static MangaManager mangaMgr;
	static ReviewManager reviewMgr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbMgr			= new DbManager();
		addedMangas		= new ArrayList<>();
		addedReviews	= new ArrayList<>();
		mangaMgr		= dbMgr.getMangaManager();
		reviewMgr		= dbMgr.getReviewManager();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		if (mangaMgr != null)		
			for (Manga dm : addedMangas)
				mangaMgr.delete(dm);
	}
	
	@Test
	public void test0Connection() {
		assertNotNull(dbMgr.getConnection());
	}

	@Test
	public void test1InsertMangas() {
		// Some fake manga information
		String mangaTitleTpl	= "Nonexistent Manga Vol. ";
		Integer mangaYear		= 2005;
		String mangaSynopsis	= "The synopsis to a nonexistent manga.";
		String mangaTags		= "sample-tag genre-maybe whatever etc";
		
		// Inserting three mangas
		for (int i = 0; i < 3; i++) {
			Manga testManga = new Manga();
			
			// Filling in the information
			testManga.setTitle(mangaTitleTpl + (i+1));
			testManga.setSynopsis(mangaSynopsis);
			testManga.setYearOfPublication(mangaYear + i);
			testManga.setTags(mangaTags);
			
			// Inserting the manga
			if (mangaMgr.insertOrUpdate(testManga) == 1) {
				addedMangas.add(testManga);
				
				// If the manga was inserted,
				// the primary key would be automatically
				// set with the autogenerated key.
				ArrayList<Manga> insertedMangas = mangaMgr.get(testManga.getMangaId());
				if (insertedMangas.size() == 1)
					assertEquals(testManga, insertedMangas.get(0));
				else if (insertedMangas.size() == 0)
					fail("Could not find an inserted manga.");
				else
					fail("The TableManager returned more than one manga (?)");
			}
			else fail("Could not insert a manga.");
		}
	}
	
	@Test
	public void test2InsertReviews() {
		// Some fake review data
		String reviewTitleTpl	= "A fake review of ";
		String reviewComments	= "Comments comments comments to ";
		Date dateOfPosting		= Date.valueOf(LocalDate.now());
		Double score			= 2.5;
		
		// Inserting reviews for previously
		// inserted mangas
		if (addedMangas.size() == 0)
			fail("There were no mangas in the array");
		else {
			for (Manga reviewedManga : addedMangas) {
				Review testReview = new Review();
				
				// Filling in the review data
				testReview.setMangaId(reviewedManga.getMangaId());
				testReview.setTitle(reviewTitleTpl + reviewedManga.getTitle());
				testReview.setDateOfPosting(dateOfPosting);
				testReview.setScoreForArt(score);
				testReview.setScoreForPlot(score);
				testReview.setScoreForDialogues(score);
				testReview.setComments(reviewComments + reviewedManga.getTitle());
				
				// Inserting the review
				if (reviewMgr.insertOrUpdate(testReview) == 1) {
					addedReviews.add(testReview);
					
					// Checking integrity of data
					ArrayList<Review> insertedReviews = reviewMgr.get(testReview.getReviewId());
					if (insertedReviews.size() == 1)
						assertEquals(testReview, insertedReviews.get(0));
					else if (insertedReviews.size() == 0)
						fail("Could not find an inserted review in the DB");
					else
						fail("The TableManager returned more than one review (?)");
				}
				else fail("Could not insert a review.");
			}
		}
	}

}
