package pl.arma.tud_jdbc;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import pl.arma.tud_jdbc.db.DbManager;
import pl.arma.tud_jdbc.db.MangaManager;
import pl.arma.tud_jdbc.db.ReviewManager;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

public class DeletionTest {
	static DbManager dbMgr;
	static MangaManager mangaMgr;
	static ReviewManager reviewMgr;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbMgr			= new DbManager();
		mangaMgr		= dbMgr.getMangaManager();
		reviewMgr		= dbMgr.getReviewManager();
	}
	
	@Test
	public void testDeletion() {
		Manga m1 = new Manga();
		Review r1 = new Review();
		
		// This test inserts a manga and
		// a review to the DB, and then
		// tries to delete the manga.
		// Deleting manga should automatically
		// delete the linked review.
		m1.setTitle("In the DB, not for long");
		m1.setYearOfPublication(2016);
		
		// Number of inserted rows should be 1
		assertEquals(1, mangaMgr.insertOrUpdate(m1));
		
		r1.setTitle("Excellent deleting skills");
		r1.setMangaId(m1.getMangaId());
		r1.setDateOfPosting(Date.valueOf(LocalDate.now()));
		r1.setScoreForArt(5.0);
		r1.setScoreForPlot(5.0);
		r1.setScoreForDialogues(5.0);
		
		// Number of inserted rows should be 1 as well
		assertEquals(1, reviewMgr.insertOrUpdate(r1));
		
		// Adding a record that should stay
		// in the DB after some deleting
		Manga m2 = new Manga();
		Review r2 = new Review();
		
		m2.setTitle("Manga that should not be deleted");
		m2.setYearOfPublication(2016);
		
		// Number of inserted rows should be 1
		assertEquals(1, mangaMgr.insertOrUpdate(m2));
		
		r2.setTitle("A review that should stay too");
		r2.setMangaId(m2.getMangaId());
		r2.setDateOfPosting(Date.valueOf(LocalDate.now()));
		r2.setScoreForArt(3.0);
		r2.setScoreForDialogues(4.0);
		r2.setScoreForPlot(5.0);
		
		// Number of inserted rows should be 1 as well
		assertEquals(1, reviewMgr.insertOrUpdate(r2));
		
		// Now try to delete the manga...
		// The number of deleted rows should be 2
		assertEquals(2, mangaMgr.delete(m1));
		
		// Check to see if the manga's been deleted
		assertEquals(0, mangaMgr.get(m1.getMangaId()).size());
		// Check to see if the review's been deleted as well
		assertEquals(0, reviewMgr.get(r1.getReviewId()).size());
		
		// Check to see if what should stay in DB
		// is actually still in the DB
		ArrayList<Manga> mangaList = mangaMgr.get(m2.getMangaId());
		ArrayList<Review> reviewList = mangaMgr.getReviews(m2);
		
		assertEquals(1, mangaList.size());
		assertEquals(m2, mangaList.get(0));
		assertEquals(1, reviewList.size());
		assertEquals(r2, reviewList.get(0));

		// Cleaning after the test
		// (will also delete the second review)
		mangaMgr.delete(m2);
	}

}
