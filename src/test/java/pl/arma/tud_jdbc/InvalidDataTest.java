package pl.arma.tud_jdbc;

import static org.junit.Assert.*;

import java.sql.Date;
import java.time.LocalDate;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import pl.arma.tud_jdbc.db.DbManager;
import pl.arma.tud_jdbc.db.MangaManager;
import pl.arma.tud_jdbc.db.ReviewManager;
import pl.arma.tud_jdbc.db.entity.Manga;
import pl.arma.tud_jdbc.db.entity.Review;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InvalidDataTest {
	static DbManager dbMgr;
	static MangaManager mangaMgr;
	static ReviewManager reviewMgr;
	static Manga insertedManga;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		dbMgr			= new DbManager();
		mangaMgr		= dbMgr.getMangaManager();
		reviewMgr		= dbMgr.getReviewManager();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if (insertedManga != null)
			mangaMgr.delete(insertedManga);
	}

	@Test
	public void testInvalidMangaInsertion() {
		Manga invalid = new Manga();
		int numberOfExceptions = 0;
		
		// Try to set invalid title
		// (i.e. shorter than 3 characters)
		try {
			invalid.setTitle("ab");
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		// Try to set invalid year of publication
		// (i.e. not in 1990-NOW range)
		try {
			invalid.setYearOfPublication(1847);
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		// The number of exceptions should be 2
		assertEquals(2, numberOfExceptions);
		
		// Try to insert such invalid entry
		// The number of inserted rows should be 0
		assertEquals(0, mangaMgr.insertOrUpdate(invalid));
		
		// Now try to insert a valid manga entry,
		// and then proceed to try to change the 
		// primary key after insertion
		Manga valid = new Manga();
		
		valid.setTitle("Abc");
		valid.setYearOfPublication(1995);
		
		// The number of inserted rows should be 1
		assertEquals(1, mangaMgr.insertOrUpdate(valid));
		
		insertedManga = valid;
		
		// Trying to change the autoassigned PK
		boolean exceptionTriggered = false;
		
		try {
			valid.setMangaId(123);
		}
		catch (UnsupportedOperationException e) {
			exceptionTriggered = true;
		}
		
		assertTrue(exceptionTriggered);
	}

	@Test
	public void testInvalidReviewInsertion() {
		Review invalid = new Review();
		int numberOfExceptions = 0;
		
		// Try to set invalid title
		// (i.e. shorter than 3 characters)
		try {
			invalid.setTitle("xy");
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		// Try to set invalid date of posting
		// (a future one - 2100-10-21)
		try {
			invalid.setDateOfPosting(new Date(2100, 10, 21));
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		// Try to set invalid scores
		// (i.e. values not in 0-5 range)
		try {
			invalid.setScoreForArt(-2.1);
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		try {
			invalid.setScoreForPlot(7.23);
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		try {
			invalid.setScoreForDialogues(5.1);
		}
		catch (IllegalArgumentException e) {
			++numberOfExceptions;
		}
		
		// At this point, the number of
		// exceptions should be 5
		assertEquals(5, numberOfExceptions);
		
		// Try inserting the invalid review
		// The number of inserted rows should be 0
		assertEquals(0, reviewMgr.insertOrUpdate(invalid));
		
		// Now try to insert a valid one, 
		// and then change the PK
		Review valid = new Review();
		
		valid.setTitle("Xyz");
		valid.setMangaId(insertedManga.getMangaId());
		valid.setDateOfPosting(Date.valueOf(LocalDate.now()));
		valid.setScoreForArt(3.2);
		valid.setScoreForPlot(4.8);
		valid.setScoreForDialogues(4.0);
		
		// The number of inserted rows
		// should now be 1
		assertEquals(1, reviewMgr.insertOrUpdate(valid));
	}
}
